function partition(arr, callback) {
    let arr1 = [];
    let arr2 = [];

    for (let index = 0; index < arr.length; index++) {
        if (callback(arr[index])) {
            arr1.push(arr[index])
        } else {
            arr2.push(arr[index])
        }
    }
    return [arr1, arr2]

}

module.exports = partition;