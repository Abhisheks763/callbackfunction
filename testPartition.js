const partition = require('./function')

const arr = [1,2,3,4,5,6,7,8];

const names = ['Elie', 'Colt', 'Tim', 'Matt'];

function isEven(val){
    return val % 2 === 0;
}

function isLongerThanThreeCharacters(val){
    return val.length > 3;
}




console.log(partition(arr, isEven));
console.log(partition(names, isLongerThanThreeCharacters))